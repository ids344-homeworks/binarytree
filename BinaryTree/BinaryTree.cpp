// IDS344 - 2022-01 - Grupo 1 - Binary tree manager
//
// Nikita Kravchenko - 1101607
// Omar N��ez - 1101587
// Oliver Infante - 1100292
// Lervis Pinales - 1096323
// Daniel B�ez - 1073597
//
//  17 de marzo del 2022


#include <iostream>
#include <string> 
#include <vector>

using namespace std;

struct BinaryTreeNode;
struct BinaryTreeNode* CreateBinaryTreeNode();
void Insert(BinaryTreeNode*& node, int data);

void DisplayBinaryTree(BinaryTreeNode* node);

void RemoveFromBinaryTree(BinaryTreeNode*& node, int data);
void SeekAndRemove(BinaryTreeNode*& node, int data);
bool RemoveByConditions(BinaryTreeNode*& node, int data);

void PrintPreorden(BinaryTreeNode*& node);
void PrintInorden(BinaryTreeNode* node);
void PrintPostorden(BinaryTreeNode* node);

void AutoFill(BinaryTreeNode*& node);

void DisplayBinaryTreeMenu();
bool IsNumber(string s);
void ExitProgram();
void ClearConsole(string textToShow);
void Pause();

string programName = "Binary Tree Manager";
BinaryTreeNode* RootNode = CreateBinaryTreeNode();
bool WasNodeDeleted = false;

int main()
{
    RootNode = NULL;

    while (true)
    {
        DisplayBinaryTreeMenu();
    }
}

struct BinaryTreeNode {
    int data;
    BinaryTreeNode* left;
    BinaryTreeNode* right;
};

struct BinaryTreeNode* CreateBinaryTreeNode()
{
    struct BinaryTreeNode* newNode = new (struct BinaryTreeNode);

    newNode->data = NULL;

    newNode->left = NULL;
    newNode->right = NULL;

    return newNode;
}

int GetUserInput()
{
    int data = 0;
    string userInput = "a";

    cout << "Type a number :\n";

    while (!IsNumber(userInput))
    {
        cin >> userInput;
    }

    data = stoi(userInput);

    return data;
}

void Insert(BinaryTreeNode*& node, int data)
{
    if (node == NULL)         //if (node->data == NULL)
    {
        node = CreateBinaryTreeNode();
    
        cout << "" << data << " was sucesfully added!\n";
        node->data = data;

        return;
    }

    if (node->data == data)
    {
        cout << "\nOoops! Number " << data << " already exists in a tree.";
        return;
    }

    BinaryTreeNode* NodeToInsert = CreateBinaryTreeNode();
    BinaryTreeNode* auxSearcher = CreateBinaryTreeNode();
    auxSearcher = node;

    NodeToInsert->data = data;

    do                                              //Checkeando cada direcci�n del arbol
    {
        if (data < auxSearcher->data)  //OjO no hay que correr todo el arbol, solamente guiamos si el valor es mas o menos que anterior
        {
            if (auxSearcher->left == NULL)
            {
                auxSearcher->left = NodeToInsert;
                cout << data << " was sucesfully added!\n";

                return;
            }               

            else if (auxSearcher->left->data == data)
            {
                cout << data << " already exists in a tree!!!";
                auxSearcher = NULL;
            }

            else
                auxSearcher = auxSearcher->left;
        }

        else
        {
            if (auxSearcher->right == NULL)
            {
                auxSearcher->right = NodeToInsert;
                cout << data << " was sucesfully added!\n";

                return;
            }

            else if (auxSearcher->right->data == data)
            {
                cout << data << " already exists in a tree!!!";
                auxSearcher = NULL;
            }

            else
                auxSearcher = auxSearcher->right;
        }
    } while (auxSearcher != NULL); 
}

void AutoFill(BinaryTreeNode*& node)
{
    int dataToInsert[15] = { 25,15,50,10,22,35,70,4,12,18,24,31,44,66,90 };
    
    for (int i = 0; i < 15; i++)
        Insert(node, dataToInsert[i]);
}

void RemoveFromBinaryTree(BinaryTreeNode*& node, int data)
{
    BinaryTreeNode* auxSearcher = CreateBinaryTreeNode();

    SeekAndRemove(node, data);

    if (!WasNodeDeleted)
        cout << "\nValue " << data << " is not stored in our binary tree.\n";

    WasNodeDeleted = false; //reiniciamos nuestro auxiliar para ver si elemento fue eliminado
}

void SeekAndRemove(BinaryTreeNode*& node, int data) //remove que utiliza estructura del sorteo preorden 
{
    BinaryTreeNode* auxSearcher = CreateBinaryTreeNode();
    BinaryTreeNode* auxSearcherLast = CreateBinaryTreeNode();

    if (node == NULL)
        return;

    if (RemoveByConditions(node, data) )  //true if node on the left was found and deleted. 
    {   
        WasNodeDeleted = true;            //Para hacer return de funcion padre
        return;            
    }
   
    SeekAndRemove(node->left, data);
    SeekAndRemove(node->right, data);

    return;
}

bool RemoveByConditions(BinaryTreeNode*& node, int data) // sin referencia "&" no se  elimina el nodo
{                                                                        
    BinaryTreeNode* auxSearcher = CreateBinaryTreeNode();
    BinaryTreeNode* auxSearcherLast = CreateBinaryTreeNode(); //se explica al fin

    if (node != NULL) 
    {
        if (node->data == data)
        {
            auxSearcher = node;

            cout << "Number " << node->data << " was succesfully deleted!";

            if (node->right == NULL && node->left == NULL) //Si el nodo es el ultimo
            {
                node = NULL;
                delete auxSearcher;                
                return true;
            }

            else if (node->left != NULL && node->right == NULL) //Si hay nodo a la izquierda
            {
                auxSearcher = node->left;
                node->data = auxSearcher->data;

                delete auxSearcher;
                node->left = NULL;
                return true;
            }

            else if (node->left == NULL && node->right != NULL) //Si hay nodo a la derecha
            {
                auxSearcher = node->right;
                node->data = auxSearcher->data;

                delete auxSearcher;
                node->right = NULL;
                return true;
            }

            else if (node->left != NULL && node->right != NULL) //Si hay nodo a la derecha y a la izquierda
            {
                auxSearcher = node->right;  //primero a la derecha

                if (auxSearcher->left == NULL) //si nodo tiene solo 2 hijos
                {
                    node->data = auxSearcher->data;

                    delete auxSearcher;
                    node->right = NULL;
                    return true;
                }

                while (true) //Si tiene mas, tomamos primero hijo a la derecha(arriba) y seguimos hasta el fin a la izquierda
                {
                    if (auxSearcher->left == NULL)
                        break;

                    auxSearcherLast = auxSearcher;                      //Last nececitamos para guardar el penultimo 
                    auxSearcher = auxSearcher->left;                    //nodo y quitar de el referencia al �ltimo, que eliminamos
                }

                node->data = auxSearcher->data;

                delete auxSearcher; //Eliminar el �ltimo nodo, asi que pasamos con referencia, al eliminar aux, se elimina el original
                auxSearcherLast->left = NULL;
                return true;
            }                      
        }
    }
    return false;
}

void PrintPreorden(BinaryTreeNode*& node)
{
    if (node == NULL)
        return;

    cout << node->data << " ";

    PrintPreorden(node->left);

    PrintPreorden(node->right);
}

void PrintInorden(BinaryTreeNode* node)
{
    if (node == NULL)
        return;

    PrintInorden(node->left);

    cout << node->data << " ";

    PrintInorden(node->right);
}

void PrintPostorden(BinaryTreeNode* node)
{
    if (node == NULL)
        return;

    PrintPostorden(node->left);

    PrintPostorden(node->right);

    cout << node->data << " ";
}


//Para mostrar cada elemento con relaci�n padre - hijos
void DisplayBinaryTree(BinaryTreeNode* node)  //ayuda mucho a visualizar como se vinculan los nodos
{
    if (node == NULL)
        return;

    string childLeft = "NULL";
    string childRight = "NULL";

    if (node->left != NULL)
        childLeft = to_string(node->left->data);
    else
        childLeft = "NULL";

    if (node->right != NULL)
        childRight = to_string(node->right->data);
    else
        childRight = "NULL";

    cout << "Node: " << node->data << ": left child: " << childLeft << " right child: " << childRight << endl;

    DisplayBinaryTree(node->left);

    DisplayBinaryTree(node->right);
}

void DisplayBinaryTreeMenu()
{
    string menuOptionString;
    int menuOption = 0;
    

    ClearConsole(programName);

    cout << "Select an option. (1 - 4)\n";
    cout << "1. Insert\n";
    cout << "2. Delete\n";
    cout << "3. Show (PreOrden)\n";
    cout << "4. Show (InOrden)\n";
    cout << "5. Show (PostOrden)\n";
    cout << "6. Show with roletions (Testing purpose)\n"; //Testing porque no es uno de los m�todos estandar para mostrar el arbol
    cout << "7. Autofill\n";
    cout << "8. Exit\n\n";
    cout << ">> ";

    cin >> menuOptionString;

    menuOption = IsNumber(menuOptionString) ? stoi(menuOptionString) : 0;

    switch (menuOption)
    {
    case 1:
        ClearConsole(programName);
        cout << "Lets insert some number into our wonderfull tree!\n";

        Insert(RootNode, GetUserInput());

        Pause();
        break;
    case 2:
        ClearConsole(programName);

        if (RootNode == NULL)
            cout << "Ooooops!!! Binary tree is empty\n";
        else
        {
            cout << "Lets delete some number from our wonderfull tree!\n";
            RemoveFromBinaryTree(RootNode, GetUserInput());
        }

        Pause();
        break;
        
        break;
    case 3:
        ClearConsole(programName);

        if ( RootNode == NULL)      
            cout << "Ooops! Binary tree is empty!\n";        
        else       
            PrintPreorden(RootNode);
        
        Pause();
        break;
    case 4:
        ClearConsole(programName);

        if (RootNode == NULL)
            cout << "Ooops! Binary tree is empty!\n";
        else
            PrintInorden(RootNode);
        
        Pause();

        break;
    case 5:
        ClearConsole(programName);

        if (RootNode == NULL)
            cout << "Ooops! Binary tree is empty!\n";
        else
            PrintPostorden(RootNode);

        Pause();
        break;

    case 6:
        ClearConsole(programName);

        if (RootNode == NULL)
            cout << "Ooops! Binary tree is empty!\n";
        else
            DisplayBinaryTree(RootNode);

        Pause();
        break;

    case 7:
        ClearConsole(programName);
        
        cout << "Lets save our time and to fill our beautifull tree with datos from the presentation!\n";

        AutoFill(RootNode);

        Pause();
        break;
    case 8:
        exit(0);
        break;
    default:
        cout << "Please, enter a valid option. (1 - 8)\n";
        Pause();
        break;
    }

    ClearConsole(programName);
}

// Checks if string is a number
bool IsNumber(string s)
{
    for (int i = 0; i < s.length(); i++)
        if (isdigit(s[i]) == false)
            return false;

    return true;
}

// Closes program.
void ExitProgram()
{
    exit(EXIT_SUCCESS);
}

// Closes program.
void ClearConsole(string textToShow)
{
    system("cls");
    cout << "-- " << textToShow << " --\n\n";
}

//Waits for a user to press some button
void Pause()
{
    cout << "\n";
    system("pause");
}